Feature: Searching Blibli in duckduckgo

  Scenario: Search Blibli Wikipedia in duckduckgo
    Given user open duckduckgo homepage
    When user type "blibli.com wikipedia" in search box
    And user click wikipedia link
    Then user should able to see "Blibli.com adalah salah satu situs web perdagangan elektronik di Indonesia."
