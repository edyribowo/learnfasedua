package org.example.driver;

import io.github.bonigarcia.wdm.WebDriverManager;
import net.thucydides.core.webdriver.DriverSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.opera.OperaDriver;

public class CustomChromeDriver implements DriverSource {
    @Override
    public WebDriver newDriver() {
        WebDriverManager.operadriver().setup();
        OperaDriver operaDriver = new OperaDriver();
        operaDriver.manage().window().maximize();
        return operaDriver;
    }

    @Override
    public boolean takesScreenshots() {
        return false;
    }
}
