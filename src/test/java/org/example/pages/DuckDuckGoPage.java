package org.example.pages;

import net.bytebuddy.implementation.bind.annotation.Default;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://duckduckgo.com/")
public class DuckDuckGoPage extends PageObject {
    @FindBy(name = "q")
    private WebElementFacade txtSearch;

    @FindBy(name = "q")
    private WebElementFacade textSearch;

    @FindBy(xpath = "//div[@id='r1-0']//h2[@class='result__title']//a[1]")
    private WebElementFacade resultBlibliSearch;

    public void searching(String keywoard) {
//        txtSearch.typeAndEnter(keywoard);
        textSearch.typeAndEnter(keywoard);
    }

    public void clickBlibliLink() {
        resultBlibliSearch.click();
    }
}
