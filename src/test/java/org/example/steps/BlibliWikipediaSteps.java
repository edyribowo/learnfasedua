package org.example.steps;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.example.pages.DuckDuckGoPage;
import org.example.pages.WikipediaPage;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Assert;

public class BlibliWikipediaSteps {
    private DuckDuckGoPage duckDuckGoPage;
    private WikipediaPage wikipediaPage;

    @Given("^user open duckduckgo homepage$")
    public void userOpenDuckduckgoHomepage() {
        duckDuckGoPage.open();
    }

    @When("^user type \"([^\"]*)\" in search box$")
    public void userTypeInSearchBox(String keyword) {
        duckDuckGoPage.searching(keyword);
    }

    @And("^user click wikipedia link$")
    public void userClickWikipediaLink() {
        duckDuckGoPage.clickBlibliLink();
    }


    @Then("^user should able to see \"([^\"]*)\"$")
    public void userShouldAbleToSee(String expected)  {
        MatcherAssert.assertThat("Wikipedia is not open",
                wikipediaPage.getBodyText(), Matchers.containsString(expected));
    }
}
