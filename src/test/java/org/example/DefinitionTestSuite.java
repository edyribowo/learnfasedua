package org.example;

import cucumber.api.CucumberOptions;
import cucumber.api.java.en.Given;import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features="src/test/resources/features/consult_dictionary/BlibliWikipedia.feature")
public class DefinitionTestSuite {
}
